<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laundrix</title>
    <link rel="stylesheet" href="tampilanadmin.css">
</head>
<body>
     
    <div class="badan-utama">
        
        <header>
            <h2>ADMIN</h2>
            <p>LAUNDRIX</p>
        </header>
 
        <nav class="navigasi">
            <ul>
                <li><a class="active" href="halaman_admin.php">Home</a></li>
                <li><a class="active" href="#pesan">Pemesanan</a></li>
                <li><a href="admin_index_laundry.php">Laundry</a></li>
                <li><a href="admin_logout.php">Logout</a></li>
            </ul>
        </nav>
        
        <div class="menu-tengah">
            <div class="kotak">
                <center>
                <h3>
                    Selamat Datang Di Halaman Admin

                </h3>
                </center>
            </div>
        </div>
 
        <footer>
            Copyright &copy; 2021 || Amanda Fidelia Rasesa
        </footer>
 
    </div>
 
</body>
</html>