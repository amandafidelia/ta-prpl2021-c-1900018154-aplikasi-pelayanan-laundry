<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laundrix</title>
    <link rel="stylesheet" href="tampilanadmin.css">
</head>
<body>
     
    <div class="badan-utama">
        
        <header>
            <h2>ADMIN</h2>
            <p>LAUNDRIX</p>
        </header>
 
        <nav class="navigasi">
            <ul>
                <li><a class="active" href="halaman_admin.php">Home</a></li>
                <li><a class="active" href="admin_index_pemesanan.php">Pemesanan</a></li>
                <li><a href="admin_index_laundry.php">Laundry</a></li>
                <li><a href="admin_logout.php">Logout</a></li>
            </ul>
        </nav>
        
        <div class="menu-tengah">
            <div class="kotak">
                <center>
                <h1><b>Data Pemesanan</b></h1><hr>

<table border="1" cellpadding="8" class="table">
<thead>
        <tr>
            <th bgcolor="#3b6878">No</th>
            <th bgcolor="#3b6878">Id Pesan</th>
            <th bgcolor="#3b6878">Nama</th>
            <th bgcolor="#3b6878">Alamat</th>
            <th bgcolor="#3b6878">Tanggal</th>
            <th bgcolor="#3b6878">Jenis Laundry</th>
            <th bgcolor="#3b6878">Banyak</th>
            <th bgcolor="#3b6878">Harga</th>
            <th bgcolor="#3b6878">Layanan</th>
            <th bgcolor="#3b6878">Total</th>
            <th bgcolor="#3b6878">Aksi</th>
        </tr>

        <script type="text/javascript" language="JavaScript"> // fungsi dalam js untuk konfirmasi saat ingin menghapus gambar
        function konfirmasi(){
            tanya = confirm("Anda Yakin Akan Menghapus Data Pemesanan Ini ?");
            if (tanya == true) 
                return true;
            else 
                return false;
        }
        </script>

    </thead>
    <tbody>
        <?php
        include "koneksi.php";
        $sql = "SELECT * FROM pemesanan";
        $query = mysqli_query($connect, $sql);

        $no = 1;
        $row = mysqli_num_rows($query); 

        if($row > 0){ 
        while($beli = mysqli_fetch_array($query)){
            echo "<tr>";
            echo "<td>".$no."</td>";
            echo "<td>".$beli['id_pesan']."</td>";
            echo "<td>".$beli['nama']."</td>";
            echo "<td>".$beli['alamat']."</td>";
            echo "<td>".$beli['tanggal']."</td>";
            echo "<td>".$beli['jenis_laundry']."</td>";
            echo "<td>".$beli['banyak']."</td>";
            echo "<td>".$beli['harga']."</td>";
            echo "<td>".$beli['layanan']."</td>";
            echo "<td>".$beli['total_harga']."</td>";

            echo "<td>";
            echo "<a href='admin_hapus_pemesanan.php?id=".$beli['id_pesan']."' class='del_btn' onclick='return konfirmasi()'>Hapus</a>";
            echo "</td>";

            echo "</tr>";
            $no++;
        }
    }else{ 
        echo "<tr><td colspan='11'>Tidak ada data</td></tr>";
    }
        ?>
    </tbody>
</table>
                </center>
            </div>
        </div>
 
        <footer>
            Copyright &copy; 2021 || Amanda Fidelia Rasesa
        </footer>
 
    </div>
 
</body>
</html>
