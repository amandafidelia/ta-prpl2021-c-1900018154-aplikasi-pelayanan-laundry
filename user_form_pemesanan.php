<!DOCTYPE html>
<html>
<head>
    <title>Pesan</title>
    <link rel="stylesheet" href="tampilan_form-pesan.css">
</head>
<body>
<div id="wrapper">
<form action="#" method="POST">
<h1>Pemesanan Jasa Laundry</h1>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td> </td>
        <td>ID Pesan</td>
        <td><input type="text" name="id_pesan" size="30"/> </td>
    </tr>
    <tr>
        <td> </td>
        <td>Nama</td>
        <td><input type="text" name="nama" size="50"></td>
    </tr>
    <tr>
        <td> </td>
        <td>Alamat</td>
        <td><input type="text" name="deskripsi" size="50"></td>
    </tr>
    <tr>
        <td> </td>
        <td>Tanggal</td>
        <td><input type="date" name="tanggal" class="dateku"></td>
    </tr>
    <tr>
        <td> </td>
        <td>Jenis Laundry</td>
        <td>
            <?php
                include("koneksi.php");
                $sql_id = mysqli_query($connect, "SELECT jenis_laundry FROM tb_file");
                $ketemu = mysqli_num_rows($sql_id);

                echo "<select name=jenis_laundry class=select>";
                echo "<option>---- Pilih Jenis Laundry----</option>";
                
                if($ketemu > 0){
                    while($data = mysqli_fetch_array($sql_id)){
                        echo "<option value='$data[0]'>$data[0]</option>";
                }
                }
                echo "</select>";
                 ?>
        </td>
    </tr>
    <tr>
        <td> </td>
        <td>Banyak (kg)</td>
        <td><input type="text" name="banyak" size="50"></td>
    </tr>
    <tr>
        <td> </td>
        <td>Harga</td>
        <td><input type="text" name="harga" size="50"></td>
    </tr>
    <tr>
        <td> </td>
        <td>Layanan</td>
        <td>
            <select name="layanan" class="select">
                <option>---- Pilih Layanan----</option>
                <option>Jemput Baju</option>
                <option>Antar Sendiri</option>
                
            </select>
        </td>
    </tr>
    <tr>
        <td> </td>
        <td> </td>
        <td><input type="submit" class="buttonku" name="Submit" value="Submit">   
            <input type="reset" class="buttonku2" name="reset" value="Reset"></td>
    </tr>
</table>
</form>
</div>
        
</body>
</html>
    