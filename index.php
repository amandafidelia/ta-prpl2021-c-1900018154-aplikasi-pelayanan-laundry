<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laundrix</title>
    <link rel="stylesheet" href="tampilan.css">
</head>
<body>
     
    <div class="badan-utama">
        
        <header>
            <h2>LAUNDRIX</h2>
            <p>Layanan Laundry Online</p>
        </header>
 
        <nav class="navigasi">
            <ul>
                <li><a class="active" href="#home">Home</a></li>
                <li><a href="#profile">Profile</a></li>
                <li><a href="#laundry">Laundry</a></li>
            </ul>
        </nav>
 
        <div class="banner">
            <img src="3.png" alt="Laundry">
        </div>
 
        <div class="menu-kiri">
            <div class="kotak">
                <h3>Pemesanan Jasa</h3>
                <p>
                    Isi Form Pemesanan Jasa Laundry dengan Klik Tombol di bawah ini
                </p>
 
                <a class="tombol tombol-pesan" href="#pesan">Pesan</a>
            </div>
        </div>
 
        <div class="menu-tengah">
            <div class="kotak">
                <h3>Laundrix</h3><br>
 
                <img src="1.jpg" alt="Profile Laundry">
 
                <p>
                    Laundrix adalah suatu jasa online yang melayani berbagai jenis paket laundry.
                    Hal ini akan memudahkan para pemakai jasa laundry untuk
                    memesan layanan laundry secara praktis.

                </p>
 
                <a class="tombol tombol-lengkap" href="#home">Selengkapnya</a>
            </div>
        </div>
 
        <div class="menu-kanan">
            <div class="kotak">
                <h3>Jadwal Buka</h3>

                <h4 align="center">Jadwal Buka</h4>
                <center>
                    <p>Senin-Sabtu 08.00 WIB - 14.00 WIB</p>

                    <a class="tombol tombol-pesan" href="admin_login.php">Admin</a>
                </center>
            </div>

        </div>
 
        <footer>
            Copyright &copy; 2021 || Amanda Fidelia Rasesa
        </footer>
 
    </div>
 
</body>
</html>