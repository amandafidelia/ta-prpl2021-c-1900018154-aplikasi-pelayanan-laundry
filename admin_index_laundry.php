<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laundrix</title>
    <link rel="stylesheet" href="tampilanadmin.css">
</head>
<body>
     
    <div class="badan-utama">
        
        <header>
            <h2>ADMIN</h2>
            <p>LAUNDRIX</p>
        </header>
 
        <nav class="navigasi">
            <ul>
                <li><a class="active" href="halaman_admin.php">Home</a></li>
                <li><a class="active" href="admin_index_pemesanan.php">Pemesanan</a></li>
                <li><a href="admin_index_laundry.php">Laundry</a></li>
                <li><a href="admin_logout.php">Logout</a></li>
            </ul>
        </nav>
        
        <div class="menu-tengah">
            <div class="kotak">
                <center>
                <h1><b>Jenis Laundry</b></h1><hr>
 <a href="admin_formupload_laundry.php" class="tambah_btn">Tambah Data</a><br><br>
<table border="1" cellpadding="8" class="table">
<tr>
  <th>Klik Gambar Untuk Melihat</th>
  <th>Jenis Laundry</th>
  <th>Harga/kg</th>
  <th>Deskripsi</th>
  <th>Tindakan</th>
</tr>

<script type="text/javascript" language="JavaScript"> // fungsi dalam js untuk konfirmasi saat ingin menghapus gambar
 function konfirmasi(){
 tanya = confirm("Anda Yakin Akan Menghapus Gambar Ini ?");
 if (tanya == true) 
    return true;
 else 
    return false;
 }
</script>

<?php
// koneksi database
include "koneksi.php";

$query = "SELECT * FROM tb_file"; // Menampilkan gambar dari database
$sql = mysqli_query($connect, $query); // Eksekusi/Jalankan query dari variabel $query
$row = mysqli_num_rows($sql); // Ambil jumlah data dari hasil eksekusi $sql

if($row > 0){ // Jika jumlah data lebih dari 0 (jika datanya ada)
  while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
    // disini saya tidak menampilkan/megoutputkan ukuran dan tipe  gambarnya
    echo "<tr>";
    echo "<td><a href='file/".$data['nama_file']."'><img src='file/".$data['nama_file']."' width='120' height='120'></td>"; // menampilkan gambar dengan lebar 120px dan tinggi 120px
    echo "<td>".$data['jenis_laundry']."</td>";
    echo "<td>".$data['harga']."</td>";
    echo "<td>".$data['deskripsi']."</td>";
    echo "<td>";
    echo "<a href='admin_hapus_laundry.php?id=".$data['id']."' class='del_btn' onclick='return konfirmasi()'>Hapus</a>";
    echo "</td>";
    echo "</tr>";
  }
}else{ // Jika data gambar tidak ada
  echo "<tr><td colspan='4'>Tidak ada gambar</td></tr>";
}
?>
</table>
                </center>
            </div>
        </div>
 
        <footer>
            Copyright &copy; 2021 || Amanda Fidelia Rasesa
        </footer>
 
    </div>
 
</body>
</html>
