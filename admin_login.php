<?php
// fungsi untuk memulai session
session_start();
 
// variabel kosong untuk menyimpan pesan error
$form_error = '';
 
// cek apakah tombol sumit sudah di klik atau belum
if(isset($_POST['submit'])){
 
    // menyimpan data yang dikirim dari metode POST ke masing-masing variabel
    $username = $_POST['username'];
    $password = $_POST['password'];
 
    // validasi login benar atau salah
    if($username == 'amanda' AND $password == 'amanda'){
 
        // jika login benar maka username akan disimpan ke session kemudian akan di redirect ke halaman admin
        $_SESSION['username'] = $username;
        header('Location: halaman_admin.php');
    }else{
 
        // jika login salah maka variabel form_error diisi value seperti dibawah
        // nilai variabel ini akan ditampilkan di halaman login jika salah
        $form_error = '<p>Password atau username yang kamu masukkan salah</p>';
    }
}
 
?>

<!DOCTYPE html>
<html>
<head>
	<title>Admin Login</title>
	<style type="text/css">
		body {
			background-image: url("3.png");
			background-size: cover;
			background-color: #cccccc;
			font-family: "Segoe UI";
		}
		#wrapper {
			background-color: rgba(255, 255, 255, 0.8);
			width: 400px;
			height: 330px;
			margin-top: 120px;
			margin-left: auto;
			margin-right: auto;
			padding: 20px;
			border-radius: 30px;
		}
		input[type=text], input[type=password] {
			border: 1px solid #ddd;
			padding: 10px;
			width: 95%;
			border-radius: 2px;
			outline: none;
			margin-top: 10px;
			margin-bottom: 20px;
		}
		label, h1 {
			text-transform: uppercase;
			font-weight: bold;
		}
		h1 {
			text-align: center;
			font-size: 40px;
			color: #c9b00a;
		}
		button {
			border-radius: 10px;
			padding: 10px;
			width: 120px;
			background-color: #c9b00a;
			border: none;
			color: #fff;
			font-weight: bold;
		}
		.error {
			background-color: #f72a68;
			width: 400px;
			height: auto;
			margin-top: 20px;
			margin-left: auto;
			margin-right: auto;
			padding: 20px;
			border-radius: 4px;
			color: #fff;

		}
	</style>
</head>
<body>
	<div id="wrapper">
		<form method="POST" action="admin_login.php">
			<h1>Login Admin</h1>
			<label>Username</label>
			<input type="text" name="username" placeholder="masukkan username" required="" autofocus="">
			<label>Password </label>
			<input type="password" name="password" placeholder="masukkan password" required="" >
			<button type="submit" name="submit">LOGIN</button>
		</form>
	</div>

		<label align="center"><?php echo $form_error; ?></label>
		
</body>
</html>