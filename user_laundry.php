<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laundrix</title>
    <link rel="stylesheet" href="tampilanadmin.css">
</head>
<body>
     
    <div class="badan-utama">
        
        <header>
            <h2>LAUNDRIX</h2>
            <p>Layanan Laundry Online</p>
        </header>
 
        <nav class="navigasi">
            <ul>
                <li><a class="active" href="index.php">Home</a></li>
                <li><a href="#profile">Profile</a></li>
                <li><a href="user_laundry.php">Laundry</a></li>
            </ul>
        </nav>
        
        <div class="menu-tengah">
            <div class="kotak">
                <center>
                <h1><b>Jenis Laundry</b></h1><hr>


    <form action="user_laundry.php" method="GET" align="right">
        <input type="text" name="cari">
        <input type="submit" value="Cari">
    </form>

    <?php 
    if(isset($_GET['cari'])){ // untuk pencariannya
    $cari = $_GET['cari'];
    echo "<b>Hasil pencarian : ".$cari."</b>";
    }
    ?>

    <br>
<table border="1" cellpadding="8" class="table">
<tr>
  <th>Klik Gambar Untuk Melihat</th>
  <th>Jenis Laundry</th>
  <th>Harga/kg</th>
  <th>Deskripsi</th>
</tr>


<?php
// koneksi database
include "koneksi.php";

if(isset($_GET['cari'])){
        $cari = $_GET['cari'];
        $query = "SELECT * from tb_file where jenis_laundry like '%".$cari."%'";
        $sql = mysqli_query($connect, $query);

        }else{
        $query = "SELECT * FROM tb_file";
        $sql = mysqli_query($connect, $query);
        }


$row = mysqli_num_rows($sql); // Ambil jumlah data dari hasil eksekusi $sql

if($row > 0){ // Jika jumlah data lebih dari 0 (jika datanya ada)
  while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
    // disini saya tidak menampilkan/megoutputkan ukuran dan tipe  gambarnya
    echo "<tr>";
    echo "<td><a href='file/".$data['nama_file']."'><img src='file/".$data['nama_file']."' width='120' height='120'></td>"; // menampilkan gambar dengan lebar 120px dan tinggi 120px
    echo "<td>".$data['jenis_laundry']."</td>";
    echo "<td>".$data['harga']."</td>";
    echo "<td>".$data['deskripsi']."</td>";
    echo "</tr>";
  }
}else{ // Jika data gambar tidak ada
  echo "<tr><td colspan='4'>Tidak ada gambar</td></tr>";
}
?>
</table>
                </center>
            </div>
        </div>
 
        <footer>
            Copyright &copy; 2021 || Amanda Fidelia Rasesa
        </footer>
 
    </div>
 
</body>
</html>

